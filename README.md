# Portal de Noticias El Hocicón - Backend

Bienvenido al backend del Portal de Noticias "El Hocicón". Este proyecto proporciona la lógica de servidor para gestionar y ofrecer noticias a través de una API RESTful.

## Funcionalidades

- Gestión de noticias: Crear, leer, actualizar y eliminar noticias.
- Carga de imágenes para las noticias.
- Búsqueda y ordenación de noticias.

## Tecnologías Utilizadas

- Node.js
- NestJS (Framework para Node.js)
- PostgreSQL (Base de datos)
- TypeORM (ORM para TypeScript y JavaScript)
- Multer (Manejo de archivos para Node.js)

## Instalación

Antes de comenzar, asegúrate de tener [Node.js](https://nodejs.org/) y [PostgreSQL](https://www.postgresql.org/) instalados en tu sistema.

1. **Clona el repositorio:**

   ```bash
   git clone https://gitlab.com/Boris_Leonel/portal-noticias-backend.git
   cd portal-noticias-backend

1. **Instala las dependencias:**

   ```bash
   npm install

1. **Inicia el servidor:**
Al momento de iniciar el proyecto se ejecutaran las migraciones y seeders automaticamente
   ```bash
   npm run start
   
El backend estará disponible en http://localhost:3000.

