
### INSTALL.md

```markdown
# Instrucciones de Instalación - Portal de Noticias El Hocicón

Este documento proporciona instrucciones sobre cómo instalar y configurar el backend del Portal de Noticias "El Hocicón".

## Requisitos Previos

Asegúrate de tener instalados los siguientes elementos antes de comenzar:

- [Node.js](https://nodejs.org/) (v14.x o superior)
- [PostgreSQL](https://www.postgresql.org/) (v10.x o superior)

## Pasos de Instalación

1. **Clona el repositorio:**

   ```bash
   git clone https://gitlab.com/Boris_Leonel/portal-noticias-backend.git
   cd portal-noticias-backend

1. **Instala las dependencias:**

   ```bash
   npm install

1. **Inicia el servidor:**
Al momento de iniciar el proyecto se ejecutaran las migraciones y seeders automaticamente
   ```bash
   npm run start
   
El backend estará disponible en http://localhost:3000.