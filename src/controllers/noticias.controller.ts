// noticias.controller.ts
import { Controller, Get, Post, Put, Delete, Param, Body, UploadedFile, UseInterceptors, Query } from '@nestjs/common';
import { NoticiasService } from '../services/noticias.service';
import { Noticia } from '../entities/noticia.entity';
import { FileInterceptor } from '@nestjs/platform-express/multer';
import { diskStorage } from 'multer';
import * as path from 'path';

@Controller('noticias')
export class NoticiasController {
    constructor(private readonly noticiasService: NoticiasService) { }

    @Get()
    async findAll(): Promise<Noticia[]> {
        return this.noticiasService.findAll();
    }

    @Get(':id/ver')
    async findById(@Param('id') id: number): Promise<Noticia> {
        return this.noticiasService.findById(id);
    }

    @Post()
    @UseInterceptors(FileInterceptor('image', {
        storage: diskStorage({
            destination: './uploads',
            filename: (req, file, callback) => {
                const uniqueSuffix = Date.now();
                const fileExtension = path.extname(file.originalname);
                const newFileName = uniqueSuffix + fileExtension;
                callback(null, newFileName);
            },
        }),
    }))
    async createWithImage(@Body() noticiaData, @UploadedFile() file) {
        const nuevaNoticia = await this.noticiasService.createWithImage(noticiaData, file);
        console.log(file);
        return nuevaNoticia;
    }


    @Put(':id')
    @UseInterceptors(FileInterceptor('image', {
        storage: diskStorage({
            destination: './uploads',
            filename: (req, file, callback) => {
                const uniqueSuffix = Date.now();
                const fileExtension = path.extname(file.originalname);
                const newFileName = uniqueSuffix + fileExtension;
                callback(null, newFileName);
            },
        }),
    }))
    async updateWithImage(@Param('id') id: number, @Body() noticiaData, @UploadedFile() file) {
        const updatedNoticia = await this.noticiasService.updateWithImage(id, noticiaData, file);
        return updatedNoticia;
    }

    @Delete(':id')
    async delete(@Param('id') id: string): Promise<{ message: string }> {
        const response = await this.noticiasService.delete(Number(id));
        return response;
    }

    @Get('buscar')
    async findWithFilters(
        @Query('searchTerm') searchTerm: string | undefined, // Cambiar a string | undefined
        @Query('orderBy') orderBy: keyof Noticia,
        @Query('order') order: 'ASC' | 'DESC',
    ): Promise<Noticia[]> {
        return this.noticiasService.findWithFilters(searchTerm, orderBy, order);
    }
}
