import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Noticia } from '../entities/noticia.entity';
import * as fs from 'fs';

@Injectable()
export class SeederService {
    constructor(
        @InjectRepository(Noticia)
        private readonly noticiaRepository: Repository<Noticia>,
    ) { }

    async seedNoticias() {
        const seedFlagPath = './seed.flag';

        if (fs.existsSync(seedFlagPath)) {
            console.log('Seeders already executed. Skipping.');
            return;
        }

        // Insertar noticias de prueba
        const noticias = [
            {
                titulo: 'Noticia 1',
                imagenPrincipal: '/uploads/default.jpg',
                lugar: 'Lugar 1',
                autor: 'Autor 1',
                contenido: 'Contenido de la noticia 1',
                fechaPublicacion: '2024-01-15 09:00'
            },
            {
                titulo: 'Noticia 2',
                imagenPrincipal: '/uploads/default.jpg',
                lugar: 'Lugar 2',
                autor: 'Autor 2',
                contenido: 'Contenido de la noticia 2',
                fechaPublicacion: '2023-12-15 09:00'
            },
            {
                titulo: 'Noticia 3',
                imagenPrincipal: '/uploads/default.jpg',
                lugar: 'Lugar 3',
                autor: 'Autor 3',
                contenido: 'Contenido de la noticia 3',
                fechaPublicacion: '2023-11-15 09:00'
            },
            // Agrega más noticias según sea necesario
        ];

        await Promise.all(
            noticias.map(async (noticiaData) => {
                const noticia = this.noticiaRepository.create(noticiaData);
                await this.noticiaRepository.save(noticia);
            }),
        );

        // Crear el archivo de marca (flag) para indicar que los seeders se ejecutaron
        fs.writeFileSync(seedFlagPath, '');

        console.log('Seeders executed successfully.');
    }
}