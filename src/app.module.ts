import { Module, OnModuleInit  } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Noticia } from './entities/noticia.entity';
import { NoticiasController } from './controllers/noticias.controller';
import { NoticiasService } from './services/noticias.service';
import { MulterModule } from '@nestjs/platform-express';

import { SeederService } from './sedeers/seeder.service';

// import { AppController } from './app.controller';
// import { AppService } from './app.service';
import * as ormconfig from '../ormconfig'; // Asegúrate de que la ruta sea correcta


@Module({
  imports: [TypeOrmModule.forRoot(ormconfig), 
    TypeOrmModule.forFeature([Noticia]),
    MulterModule.register({
      dest: './uploads', // Ruta donde se guardarán los archivos
    }),],
  controllers: [NoticiasController],
  providers: [NoticiasService, SeederService],
})
export class AppModule implements OnModuleInit  {
  constructor(private readonly seederService: SeederService) {
    // Llama al método seedNoticias durante la inicialización de la aplicación
  }

  async onModuleInit() {
    // Llama al método seedNoticias durante la inicialización de la aplicación
    await this.seederService.seedNoticias();
    // Agrega llamadas a otros métodos de seeders si es necesario
  }
}
