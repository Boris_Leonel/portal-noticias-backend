// noticias.service.ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository, Like, FindManyOptions, OrderByCondition } from 'typeorm';
import { Noticia } from '../entities/noticia.entity';
import { NotFoundException } from '@nestjs/common';

@Injectable()
export class NoticiasService {
  constructor(
    @InjectRepository(Noticia)
    private noticiasRepository: Repository<Noticia>,
  ) { }

  async findAll(): Promise<Noticia[]> {
    return this.noticiasRepository.find();
  }

  async findById(id: number): Promise<Noticia> {
    try {
      const existeNoticia = await this.noticiasRepository.findOneOrFail({
        where: { id }, // Proporciona el id en las opciones de búsqueda
      });
      return existeNoticia;
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new NotFoundException('Noticia no encontrada');
      }
      throw error; // Reenvía el error si no es del tipo EntityNotFoundError
    }
  }

  async create(noticia: Partial<Noticia>): Promise<Noticia> {
    const nuevaNoticia = this.noticiasRepository.create(noticia);
    return this.noticiasRepository.save(nuevaNoticia);
  }

  async createWithImage(noticia: Partial<Noticia>, file: Express.Multer.File): Promise<Noticia> {

    noticia.imagenPrincipal = '/uploads/' + file.filename; // Aquí asumo que solo estás manejando una imagen principal, ajusta según tus necesidades
    const nuevaNoticia = this.noticiasRepository.create(noticia);

    return this.noticiasRepository.save(nuevaNoticia);
  }

  async updateWithImage(id: number, noticiaData: Partial<Noticia>, file: Express.Multer.File): Promise<Noticia> {
    try {
      const noticiaToUpdate = await this.noticiasRepository.findOneOrFail({
        where: { id }, // Proporciona el id en las opciones de búsqueda
      });
      // Actualiza la información de la noticia
      const updatedNoticia = Object.assign(noticiaToUpdate, noticiaData);

      // Si hay una nueva imagen, guarda la nueva imagen y actualiza la ruta en la noticia
      if (file) {
        updatedNoticia.imagenPrincipal = '/uploads/' + file.filename;
      }

      // Guarda la noticia actualizada en la base de datos
      return this.noticiasRepository.save(updatedNoticia);
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new NotFoundException('Noticia no encontrada');
      }
      throw error; // Reenvía el error si no es del tipo EntityNotFoundError
    }

  }

  async delete(id: number): Promise<{ statusCode: number; message: string }> {
    try {
      const existeNoticia = await this.noticiasRepository.findOneOrFail({
        where: { id }, // Proporciona el id en las opciones de búsqueda
      });
      await this.noticiasRepository.delete(id);

      return { statusCode: 200, message: 'Noticia eliminada exitosamente' };

    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new NotFoundException('Noticia no encontrada');
      }
      throw error; // Reenvía el error si no es del tipo EntityNotFoundError
    }
  }

  async findWithFilters(
    searchTerm?: string,
    orderBy?: keyof Noticia,
    order?: 'ASC' | 'DESC',
  ): Promise<Noticia[]> {
    const where: FindManyOptions<Noticia>['where'] = {};
    const orderCondition: OrderByCondition = {};

    if (searchTerm) {
      return this.addSearchConditionsAndOrder(searchTerm, orderBy || 'fechaPublicacion', order);
    }

    if (orderBy) {
      orderCondition[orderBy] = order || 'ASC';
    }

    return this.noticiasRepository.find({
      where,
      order: orderCondition,
    });
  }
  
  private addSearchConditionsAndOrder(searchTerm: string, orderField: string, orderDirection: 'ASC' | 'DESC'): Promise<Noticia[]> {
    const lowercaseSearchTerm = searchTerm.toLowerCase(); // Convertir el término de búsqueda a minúsculas
  
    return this.noticiasRepository.createQueryBuilder('noticia')
      .where('LOWER(noticia.titulo) LIKE :searchTerm', { searchTerm: `%${lowercaseSearchTerm}%` })
      .orWhere('LOWER(noticia.autor) LIKE :searchTerm', { searchTerm: `%${lowercaseSearchTerm}%` })
      .orWhere('LOWER(noticia.lugar) LIKE :searchTerm', { searchTerm: `%${lowercaseSearchTerm}%` })
      .orWhere('LOWER(noticia.contenido) LIKE :searchTerm', { searchTerm: `%${lowercaseSearchTerm}%` })
      .orderBy(`noticia.${orderField}`, orderDirection)
      .getMany();
  }
  
}
