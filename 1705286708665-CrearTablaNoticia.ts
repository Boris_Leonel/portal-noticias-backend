import { MigrationInterface, QueryRunner } from "typeorm";

export class CrearTablaNoticia1705286708665 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          CREATE TABLE "noticia" (
            "id" SERIAL NOT NULL,
            "titulo" character varying NOT NULL,
            "imagenPrincipal" character varying NOT NULL,
            "fechaPublicacion" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
            "lugar" character varying NOT NULL,
            "autor" character varying NOT NULL,
            "contenido" text NOT NULL,
            CONSTRAINT "PK_id" PRIMARY KEY ("id")
          )
        `);
      }
    
      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
          DROP TABLE "noticia"
        `);
      }
}
