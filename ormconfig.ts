// ormconfig.ts
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Noticia } from './src/entities/noticia.entity';

const config: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'admin',
  database: 'portal_noticia_pasantia',
  entities: [Noticia],
  synchronize: true,
  logging: true,
  
};

export = config;

